import os
import re
import sys

def recreateTags(str):
    pattern = r'&lt;'
    str = re.sub(pattern, '<', str)

    pattern = r'&gt;'
    str = re.sub(pattern, '>', str)

    return str

def br2nl(str): 
    pattern = r'<br ?/?>'
    str = re.sub(pattern, '\n', str)
    
    return str

def removeTags(str):
    tagsToRemove = ['xml', 'body']
    for tag in tagsToRemove:
        pattern = r'</?'+tag+'>'
        str = re.sub(pattern, '', str)

    return str

def removeComments(str):
    pattern = r"<!--[\s\S]*?-->"
    str = re.sub(pattern, '', str)

    return str

def cleanAttr(str):
    attrsToRemove = ['data-tag', 'style', 'class']

    for attr in attrsToRemove:
        pattern = r' '+ attr +'="[^"]+"'
        str = re.sub(pattern, '', str)

    return str

def mergeFiles(dir, ext, clean, outFormat):
    subfolders, files = [], []
    outRootDir = "_out/"
    for f in os.scandir(dir):
        if f.is_dir() and f.path != "./_out" and f.path != "./.git":
            subfolders.append(f.path)
        if f.is_file():
            if os.path.splitext(f.name)[1].lower() in ext:
                files.append(f.path)

    if files:
        files.sort()
        outFileName = outRootDir + os.path.dirname(f.path) + "/merge." + outFormat
        pattern = r'\.\/_in\/'
        outFileName = "./" + re.sub(pattern, '', outFileName)
        os.makedirs(os.path.dirname(outFileName), exist_ok=True)

        try:
            os.remove(outFileName)
        except OSError:
            pass

        outFile = open(outFileName, "a")

        if (outFormat == "xml"):
            outFile.write("<TEI>\n\t<teiHeader>\n\t</teiHeader>\n\t<text>\n\t\t<body>")
        for inFilePath in files:
            inFile = open(inFilePath, "r")
            inFileContent = inFile.read()
            if clean:
                inFileContent = removeTags(inFileContent)
                inFileContent = cleanAttr(inFileContent)
                inFileContent = recreateTags(inFileContent)
                inFileContent = removeComments(inFileContent)
                if (outFormat == "txt"):
                    inFileContent = br2nl(inFileContent)

            outFile.write(inFileContent)
            inFile.close()
        if (outFormat == "xml"):
            outFile.write("\n\t\t</body>\n\t</text>\n</TEI>")
        outFile.close()

    for dir in list(subfolders):
        mergeFiles(dir, ext, clean, outFormat)

    return True

if __name__ == "__main__":
   outFormat = sys.argv[1]
   mergeFiles("./_in", [".xml"], True, outFormat)

