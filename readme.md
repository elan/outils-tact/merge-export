# TACT - Merge export

Permet de merger les fichiers d'un même dossier, récursivement, en 1 fichier xml (1 fichier XML par dossier "terminal" en entrée)

## Utilisation
* Copier les données dézippées dans `_in/`
* Lancer
```
python3 app.py xml
```

ou 

```
python3 app.py txt
```

-> les données produites sont dans `_out/`

## Auteurs
* Arnaud Bey

## Licence
GNU AFFERO GENERAL PUBLIC LICENSE Version 3
